import argparse
import os.path as op
import json
import sys
import subprocess
import tempfile
from renderer import Renderer


def get_options(args):
    options = ''
    if args.options_path:
        try:
            with open(args.options_path) as options_file:
                options += options_file.read()
        except FileNotFoundError as e:
            print('Options file not found at %s.' % args.options_path)
            exit(1)
    if args.inline_options:
        options += args.inline_options
    return json.loads(options)


def get_css(args):
    css = ''
    if args.css_path:
        try:
            with open(args.css_path) as css_file:
                css += css_file.read()
        except FileNotFoundError:
            print('CSS file not found at %s.' % args.css_path)
            exit(1)
    if args.inline_css:
        css += args.inline_css
    return css


def get_template(args):
    TEMPLATES = {
        'merit_profile': Renderer.TPL_MERIT_PROFILE,
        'test_suite': Renderer.TPL_TEST_SUITE
    }
    template_path = TEMPLATES[args.template] if args.template in TEMPLATES else Renderer.TPL_MERIT_PROFILE
    try:
        with open(template_path) as template_file:
            template = template_file.read()
    except FileNotFoundError:
        print('Template file not found at %s.' % template_path)
        exit(1)
    return template


def get_data(args):
    try:
        with open(args.data_path) as data_file:
            data = data_file.read()
    except FileNotFoundError as e:
        print('Data file not found at %s.' % args.input_data)
        exit(1)
    return json.loads(data)


def cli() -> None:
    formatter = lambda prog: argparse.HelpFormatter(prog, max_help_position=42)

    parser = argparse.ArgumentParser(formatter_class=formatter,
                                     description='Render majority judgment merit profile data.')
    parser.add_argument('data_path',
                        help='Merit profile data path')
    parser.add_argument('-t', '--template', choices=['merit_profile', 'test_suite'], default='merit_profile',
                        help='The template to use (default: %(default)s)')

    parser.add_argument('-o', '--options_path', help='Custom options file to add')
    parser.add_argument('-O', '--inline_options', help='Custom inline options to add', default='')
    parser.add_argument('-c', '--css_path', help='Custom CSS file to add')
    parser.add_argument('-C', '--inline_css', help='Custom inline CSS to add', default='')

    parser.add_argument('-r', '--raw', action='store_true', help='Print MP raw data table')
    parser.add_argument('-p', '--png', metavar='PNG_PATH', help='Export figure to png')
    parser.add_argument('-s', '--svg', metavar='SVG_PATH', help='Export figure to svg')

    args = parser.parse_args()
    data = get_data(args)
    mpr = Renderer(get_options(args), get_css(args), get_template(args))

    if args.raw:
        print(mpr.term_table(data))
        exit(0)

    if args.png or args.svg:
        try:
            if args.svg:
                with open(args.svg, 'w') as svg_file:
                    svg_file.write(mpr.render_svg(data))
            else:
                with open(args.png, 'wb') as png_file:
                    png_file.write(mpr.render_png(data))
        except FileNotFoundError as e:
            print(e)
            exit(1)
    else:
        program = {'linux': 'xdg-open', 'win32': 'explorer', 'darwin': 'open'}
        with tempfile.NamedTemporaryFile() as temp_file:
            temp_file.write(mpr.render_png(data))
            subprocess.run([program[sys.platform], temp_file.name])


if __name__ == '__main__':
    cli()
